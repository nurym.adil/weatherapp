﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherApp.LocationApi
{
    public class TimeZone
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int GmtOffset { get; set; }
        public bool IsDaylightSaving { get; set; }
        public object NextOffsetChange { get; set; }
    }
}
