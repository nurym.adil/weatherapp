﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherApp.LocationApi
{
    public class Imperial
    {
        public int Value { get; set; }
        public string Unit { get; set; }
        public int UnitType { get; set; }
    }
}
