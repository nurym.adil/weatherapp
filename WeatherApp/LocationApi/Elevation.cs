﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherApp.LocationApi
{
    public class Elevation
    {
        public Metric Metric { get; set; }
        public Imperial Imperial { get; set; }
    }
}
