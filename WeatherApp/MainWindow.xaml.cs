﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WeatherApp.LocationApi;
using WeatherApp.WeatherApi;

namespace WeatherApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            

        
        }

        private void SetItem(ItemsControl  items,int index,Weather weather)
        {
            items.Items.Add(new TextBlock { Text= weather.DailyForecasts[index].Date.ToString(), FontSize = 20 });
            items.Items.Add(new TextBlock { Text= "День" });
            items.Items.Add(new TextBlock { Text= weather.DailyForecasts[index].Day.IconPhrase, FontSize = 20 });
            items.Items.Add(new TextBlock { Text= "Ночь" });
            items.Items.Add(new TextBlock { Text= weather.DailyForecasts[index].Night.IconPhrase, FontSize = 20 });
            items.Items.Add(new TextBlock { Text= "Мин.  температура" , FontSize=20});
            items.Items.Add(new TextBlock { Text= weather.DailyForecasts[index].Temperature.Minimum.Value.ToString(), FontSize = 20 });
            items.Items.Add(new TextBlock { Text= "Макс.  температура" , FontSize=20});
            items.Items.Add(new TextBlock { Text= weather.DailyForecasts[index].Temperature.Maximum.Value.ToString(), FontSize = 20 });;
        }
        private static Weather GetWeather(WebClient webClient, string apiKey, string locationKey)
        {
            var json = webClient.DownloadString(
                $"http://dataservice.accuweather.com/forecasts/v1/daily/5day/{locationKey}?apikey={apiKey}&language=ru-ru&details=false&metric=true");

            var weather = JsonConvert.DeserializeObject<Weather>(json);
            return weather;
        }

        private string GetLocationKey(WebClient webClient, string apiKey, string cityName)
        {
            var json = webClient.DownloadString(
                    $"http://dataservice.accuweather.com/locations/v1/cities/search?apikey={apiKey}&q={cityName}&details=false");

            var location = JsonConvert.
                DeserializeObject<List<Location>>(json).FirstOrDefault();
            return location.Key;
        }

        private void CityNameTextBoxKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                WebClient webClient = new WebClient();
                string apiKey = "rtiAV0I3xvCRuB5lytg5nHE19D28JDOZ";
                string cityName = cityNameTextBox.Text;
                var locationKey = GetLocationKey(webClient, apiKey, cityName);

                Weather weather = GetWeather(webClient, apiKey, locationKey);
                SetItem(today, 0, weather);
                SetItem(tomorrow, 1, weather);
                SetItem(afterTomorrow, 2, weather);
                SetItem(fourDay, 3, weather);
                SetItem(fiveDay, 4, weather);
            }
        }
    }
}
